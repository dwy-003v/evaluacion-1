### Primera Evaluación DWY ###
1. Para esta evaluación deberá crear un repositorio en gitlab el cual deberá compartir con el usuario se.alvarado. (Puede ser publico o privado)

2. En el proyecto, debe crear un sitio web, de un tema a elección, que contenga lo siguiente:

    1. Un encabezado con un navegador (navbar con nav) que contenga los siguientes item:

        1. Inicio
        2. Quienes Somos
        3. Contáctenos

    2. Un banner tipo carousel, con mínimo 3 imágenes que hablen del contenido de su sitio

    3. Un cuerpo que contenga:

        1. Al menos 5 tarjetas (card) de forma ordenada que entregue información con relación al tema de su sitio. Cada tarjeta debe tener:
            1. Un título
            2. Un cuerpo (con un resumen del texto)
            3. Una imagen
            4. Un pie con una fecha
        
        2. Una galería de imágenes con al menos 5 imágenes relacionadas con su sitio

        3. Un pie de página con el nombre completo del alumno que desarrollo el sitio web

3. A demás, se debe incluir las siguientes interacciones:

    1. El nav Quienes Somos, debe abrir un modal que tenga un pequeño resumen del tema del sitio web
    
    2. El nav Contáctenos, debe abrir un modal con un formulario que contenga los siguientes item:

        Dato solicitado | Validaciones requeridas
        ---|---
        Correo electrónico | 1. Requerido <br> 2. Formato de correo electrónico
        Run | 1. Requerido<br> 2. Formato RUN chileno<br> 3. Validar el digito verificador
        Nombre Completo | 1. Requerido<br> 2. Solo letras
        Fecha Nacimiento | 1. Requerido<br> 2. Solo posible seleccionar mayores de 18 años
        Teléfono de contacto | 1. Formato +569-1234-5678
        
        1. Debe estar validado con jquery validator
     
    3. Al presionar el título de una tarjeta, esta debe poder verse en un Modal, con el contenido del texto completo
    4. Al presionar una imagen de la galería, esta debe poder verse en un Modal

4. El sitio debe estar completamente construido y ordenado con elementos de boostrap, con al menos 2 tamaños de pantalla
 y debe tener al menos 2 funciones
en javascript que lo ayuden a fabricar su contenido. El sitio debe estar construido con elementos semánticos.

5. Deben tener al menos 4 commit en su proyecto, con notorio avance entre cada uno.

Link de referencias
```
https://getbootstrap.com/docs/4.5/layout/grid/
https://getbootstrap.com/docs/4.5/components/navbar/
https://getbootstrap.com/docs/4.5/components/navs/
https://getbootstrap.com/docs/4.5/components/card/
https://getbootstrap.com/docs/4.5/components/modal/
https://getbootstrap.com/docs/4.5/utilities/colors/
https://getbootstrap.com/docs/4.5/utilities/flex/
https://jquery.com/download/
https://getbootstrap.com/docs/4.5/getting-started/download/
https://jqueryvalidation.org/
```



        
